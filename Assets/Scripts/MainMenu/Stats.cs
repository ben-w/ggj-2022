using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

namespace MainMenu
{
    public class Stats : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private TextMeshProUGUI _text;

        [SerializeField]
        private bool _isUpdating = true;

        private IEnumerator Timer()
        {
            while (_isUpdating)
            {
                _text.text = $"{PhotonNetwork.CountOfPlayersOnMaster} Players in menu\n" +
                             $"{PhotonNetwork.CountOfPlayersInRooms} Players in rooms";
                // Pun has this hard coded to 5 seconds
                yield return new WaitForSeconds(5);
            }
        }

        public override void OnConnectedToMaster()
        {
            if (!_isUpdating)
                return;
            
            StartCoroutine(Timer());
        }
    }
}
