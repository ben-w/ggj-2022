using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

namespace MainMenu
{
    internal class MainMenuManager : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private string _gameVersion = "0.3.0";

        [SerializeField]
        private int _gameSceneIndex = 1;

        [SerializeField]
        private CustomButton _joinRandomButton;
        
        [SerializeField]
        private TMP_InputField _input;

        [SerializeField]
        private CustomButton _joinCodeButton;

        [SerializeField]
        private CustomButton _hostButton;

        [SerializeField]
        private TextMeshProUGUI _topErrorText;
        
        [SerializeField]
        private TextMeshProUGUI _bottomErrorText;

        [SerializeField]
        private float _errorTextTimer = 2f;

        [SerializeField]
        private GameObject _menuCanvas;

        [SerializeField]
        private GameObject _loadingCanvas;

        private void Awake()
        {
            DisableButtons();
            
            // When returning to this Main Menu scene
            // the player is already connected
            if (PhotonNetwork.IsConnected)
                return;
            Debug.Log($"Connecting to master server with version {_gameVersion}");
            PhotonNetwork.GameVersion = _gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }

        private void EnableButtons() => ToggleButtonsState(true);

        private void DisableButtons() => ToggleButtonsState(false);
        
        // Changes the state of the online buttons, so we can make sure
        // the player doesn't try to join before they have connected
        // to the master server
        private void ToggleButtonsState(bool state)
        {
            _joinRandomButton.IsEnabled = state;
            _joinCodeButton.IsEnabled = state;
            _hostButton.IsEnabled = state;
            _input.interactable = state;
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.Log($"Disconnected. Reason, {cause}", gameObject);
        }

        // Called from the UI
        public void JoinRandomRoom()
        {
            Debug.Log("Joining a random room", gameObject);
            PhotonNetwork.JoinRandomRoom();
        }

        // Called from the UI
        public void JoinWithCode()
        {
            if (!ValidInput())
            {
                Debug.LogError($"\"{_input.text}\" was not a valid join code");
                return;
            }
            
            PhotonNetwork.JoinRoom(_input.text);
        }

        // Called from the UI
        public void HostWithCode()
        {
            if (!ValidInput())
            {
                Debug.LogError($"\"{_input.text}\" was not a valid room name");
                return;
            }

            RoomOptions options = new RoomOptions
            {
                MaxPlayers = 2,
                IsOpen = true,
                IsVisible = true,
                PublishUserId = true
            };
            
            PhotonNetwork.CreateRoom(_input.text, options);
        }

        private bool ValidInput()
        {
            return !string.IsNullOrEmpty(_input.text);
        }
        
        #region Callbacks
        
        // Called after ConnectUsingSettings
        public override void OnConnectedToMaster()
        {
            Debug.Log("Connected to master server", gameObject);
            EnableButtons();
            CodeUpdated(_input.text);
        }
        
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.LogError($"Failed to join a random room. {message}");
            StartCoroutine(DisplayError(message, false));
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.LogError($"Failed to create a room. Reason = {message}");
            StartCoroutine(DisplayError(message, true));
        }

        public override void OnCreatedRoom()
        {
            Debug.Log($"Created New Room");
        }
        
        public override void OnJoinedRoom()
        {
            Debug.Log("Joined a room");
            _menuCanvas.SetActive(false);
            _loadingCanvas.SetActive(true);
            PhotonNetwork.LoadLevel(_gameSceneIndex);
        }
        
        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.LogError($"Failed to Join room. Reason = {message}");
            StartCoroutine(DisplayError(message, true));
        }

        #endregion


        public void CodeUpdated(string newCode)
        {
            bool isValid = ValidInput();

            _joinCodeButton.IsEnabled = isValid;
            _hostButton.IsEnabled = isValid;
        }

        private IEnumerator DisplayError(string message, bool displayBottom)
        {
            if (displayBottom)
            {
                _bottomErrorText.text = message;
                _input.interactable = false;
                _joinCodeButton.IsEnabled = false;
                _hostButton.IsEnabled = false;
            }
            else
            {
                _topErrorText.text = message;
                _joinRandomButton.IsEnabled = false;
            }
            
            yield return new WaitForSeconds(_errorTextTimer);
            
            if (displayBottom)
            {
                _bottomErrorText.text = string.Empty;
                _input.interactable = true;
                _joinCodeButton.IsEnabled = true;
                _hostButton.IsEnabled = true;
            }
            else
            {
                _topErrorText.text = string.Empty;
                _joinRandomButton.IsEnabled = true;
            }
        }

        public void Quit()
        {
            Application.Quit();
        }


    }
}