using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Utilities
{
    [RequireComponent(typeof(BoxCollider))]
    public class BoxColliderEvents : MonoBehaviour
    {
        public UnityEvent<Collider> TriggerEnter;
        public UnityEvent<Collider> TriggerStay;
        public UnityEvent<Collider> TriggerExit;

        public UnityEvent<Collision> CollisionEnter;
        public UnityEvent<Collision> CollisionStay;
        public UnityEvent<Collision> CollisionExit;
        
        private void OnTriggerEnter(Collider other) => TriggerEnter.Invoke(other);

        private void OnTriggerExit(Collider other) => TriggerExit.Invoke(other);

        private void OnTriggerStay(Collider other) => TriggerStay.Invoke(other);

        private void OnCollisionEnter(Collision collision) => CollisionEnter.Invoke(collision);

        private void OnCollisionStay(Collision collisionInfo) => CollisionStay.Invoke(collisionInfo);

        private void OnCollisionExit(Collision other) => CollisionExit.Invoke(other);
    }
}
