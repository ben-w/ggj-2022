using System.Collections.Generic;

namespace Utilities
{
    internal static class Extensions
    {
        // This could be improved to go further than just one index past
        public static int ClapValueToCount<T>(this List<T> list, int amount)
        {
            if (amount > list.Count - 1)
                amount = 0;
            else if (amount < 0)
                amount = list.Count - 1;

            return amount;
        }
    }
}