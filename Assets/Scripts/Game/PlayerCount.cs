using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public class PlayerCount : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private TextMeshProUGUI _text;
        
        private void Start()
        {
            UpdateText();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log($"New player joined this room. {newPlayer.UserId}");
            UpdateText();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log($"Player {otherPlayer.UserId} has left this room");
            UpdateText();
        }

        private void UpdateText()
        {
            Room currentRoom = PhotonNetwork.CurrentRoom;
            _text.text = $"{currentRoom.PlayerCount}/{currentRoom.MaxPlayers} Players";
        }
    }

}
