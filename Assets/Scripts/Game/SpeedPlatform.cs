using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class SpeedPlatform : MonoBehaviour
    {
        [Header("Settings")]

        [SerializeField, Tooltip("The new speed of the players vehicle")]
        private float _newSpeed;

        [SerializeField, Tooltip("The time it takes to get to the new speed")]
        private float _timeToSpeed;

        [SerializeField, Tooltip("How long the new speed lasts for")]
        private float _duration;

        // This is called from BoxColliderEvents
        public void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player"))
                return;

            // The box collider is on the model, so we need to go up to find the PlayerController
            PlayerController controller = other.transform.parent.parent.GetComponent<PlayerController>();

            // The speed should only change for the client,
            // then the client sends an RPC to everyone else
            if (controller == null ||
                !controller.View.IsMine)
                return;
            
            controller.ChangeSpeed(_newSpeed, _timeToSpeed, _duration);

        }
    }
}
