using System;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    internal class ReadyText : MonoBehaviourPunCallbacks
    {
        public UnityEvent EveryoneReady;
        [SerializeField]
        private PhotonView _view;
        
        [SerializeField]
        private TextMeshProUGUI _text;

        private bool _isVisible = false;
        
        /// <summary>
        /// The state of every player, UID as key and IsReady as value.
        /// </summary>
        private Dictionary<string, bool> _playersState = new Dictionary<string, bool>();

        private void Start()
        {
            UpdateVisibility();
        }

        private void Update()
        {
            if (!_isVisible)
                return;
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ReadyUp();
            }
        }

        public override void OnPlayerEnteredRoom(Player newPlayer) => UpdateVisibility();

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            _playersState.Remove(otherPlayer.UserId);
            UpdateVisibility();
        }

        private void UpdateVisibility()
        {
            Room currentRoom = PhotonNetwork.CurrentRoom;
            _isVisible = currentRoom.MaxPlayers == currentRoom.PlayerCount;
            _text.enabled = _isVisible;

            // No need to update the text if its invisible
            if (!_isVisible)
                return;

            UpdateText();
        }

        private void UpdateText()
        {
            int readyCount = 0;
            foreach (var pair in _playersState)
            {
                if (pair.Value)
                {
                    readyCount++;
                }
            }
            
            _text.text = "Press Space to ready up.\n" +
                         $"{readyCount}/{PhotonNetwork.CurrentRoom.PlayerCount} Ready";

            if (readyCount == PhotonNetwork.CurrentRoom.PlayerCount)
            {
                EveryoneReady.Invoke();
            }
        }

        private void ReadyUp()
        {
            _view.RPC(nameof(PlayerIsReady), RpcTarget.AllBuffered);
        }

        [PunRPC]
        private void PlayerIsReady(PhotonMessageInfo info)
        {
            string uid = info.Sender.UserId;
            Debug.Log($"Player {uid} is ready");
            
            
            if (_playersState.TryGetValue(uid, out bool oldValue))
            {
                _playersState[uid] = true;
            }
            else
            {
                _playersState.Add(uid, true);
            }
            
            UpdateText();
        }
    }
}