using Photon.Pun;
using UnityEngine;

namespace Game
{
    public class PlayerColour : MonoBehaviour
    {
        [SerializeField]
        private Material _main;
        
        [SerializeField]
        private Material _alt;

        [SerializeField]
        private MeshRenderer _renderer;

        [SerializeField]
        private PhotonView _view;

        [PunRPC]
        public void SetMaterial(bool isAlt)
        {
            _renderer.material = isAlt ? _alt : _main;

            if (!_view.IsMine)
                return;
            
            _view.RPC(nameof(SetMaterial), RpcTarget.OthersBuffered, isAlt);
        }
    }
}