using System;
using System.Collections;
using System.Collections.Generic;
using BansheeGz.BGSpline.Components;
using DG.Tweening;
using Photon.Pun;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using Utilities;

namespace Game
{
    public class PlayerController: MonoBehaviour
    {
        public PhotonView View => _view;
        public BGCcMath Curve;
        public bool IsActive;
        public Action OnFinished;

        [SerializeField]
        private PlayerColour _colour;

        [SerializeField]
        private Transform _transform;

        [SerializeField]
        private float _currentSpeed = 10f;

        [SerializeField]
        private float _baseSpeed = 10f;
        
        [SerializeField]
        private float _turnSpeed = 120f;

        [SerializeField]
        private float _hoverHeight = 1f;

        [SerializeField]
        private float _resetTime = 0.5f;

        [SerializeField, Tooltip("This is how far along the track before it should resync this client")]
        private float _trackResyncDistance = 20f;

        [SerializeField]
        private Transform _model;

        [SerializeField]
        private Transform _raySource;

        [SerializeField]
        private PhotonView _view;

        [SerializeField]
        private List<MeshRenderer> _renderers;

        private List<PlayerController> _otherPlayers;

        private int _currentIndex = 0;
        private float _angle;
        private float _posOnTrack = 0;
        private float _maxDistance = 0;

        private void Start()
        {
            // I don't like this, just lack of time
            Curve = FindObjectOfType<BGCcMath>();
            
            Rotate(0);
            _maxDistance = Curve.GetDistance();
        }

        private void Update()
        {
            if (!IsActive)
                return;

            // This is a one time event but it is in update because we know
            // all players will be ready when IsActive is true
            if (_otherPlayers == null)
            {
                FindOtherPlayers();
            }

            if (_view.IsMine)
            {
                if (Input.GetKey(KeyCode.D))
                {
                    Rotate(Time.deltaTime * _turnSpeed);
                }
                else if (Input.GetKey(KeyCode.A))
                {
                    Rotate(Time.deltaTime * -_turnSpeed);
                }
            }
            
            _posOnTrack += _currentSpeed * Time.deltaTime;
            UpdatePosition();
            // Looping back round the curve
            if (_posOnTrack > _maxDistance)
                Finished();

        }

        private void UpdatePosition()
        {
            Vector3 posOnTrack = Curve.CalcPositionAndTangentByDistance(_posOnTrack, out Vector3 rotation);
            transform.rotation = Quaternion.LookRotation(rotation);
            
            
            // Ray source is used to send the ray cast for the ground check
            _raySource.position = posOnTrack;
            _raySource.localRotation = Quaternion.Euler(0,0,_angle);

            Ray ray = new Ray()
            {
                origin = posOnTrack,
                direction = -_raySource.up
            };
            if (Physics.Raycast(ray, out RaycastHit hitInfo))
            {
                transform.position = hitInfo.point;
                
                // Setting the model to look correct.
                // The local rotation is already set at 180
                _model.localRotation = Quaternion.Euler(0,180,-_angle);
                _model.position = ray.GetPoint(hitInfo.distance - _hoverHeight);
            }
        }

        private void Rotate(float amount)
        {
            _angle += amount;
            _transform.rotation = Quaternion.Euler(0,0,_angle);
            _view.RPC(nameof(UpdateAngle), RpcTarget.Others, _angle);
        }
        
        // Method called changed speed with time and new speed value
        // RPC that and do it on all other clients
        // Also send current track position in case it got desynced
        [PunRPC]
        public void ChangeSpeed(float newSpeed, float timeToSpeed, float duration)
        {
            Debug.Log($"Changing speed from {_currentSpeed} to {newSpeed} in {duration} seconds", gameObject);
            DOTween.To(
                    () => _currentSpeed, 
                    x => _currentSpeed = x, 
                    newSpeed, 
                    timeToSpeed);

            StartCoroutine(ResetSpeed(duration));
            if (!_view.IsMine)
                return;
            // For the owner to update everyone
            
            _view.RPC(nameof(ChangeSpeed), RpcTarget.Others, newSpeed, timeToSpeed, duration);
            _view.RPC(nameof(ReSyncPosition), RpcTarget.Others, _angle, _posOnTrack);
        }

        // Every time we change the speed on the clients player,
        // we send a RPC to this method to everyone else 
        // in case there was some de 
        [PunRPC]
        private void ReSyncPosition(float angle, float distance)
        {
            if (_view.IsMine)
                return;

            // This code doesn't really work too well
            // It puts the player in the correct position but is 
            // delayed by ping, so they fall behind a bit. So it works for large 
            // desyncs but not small
            float differences = _posOnTrack - distance;
            
            // Don't want it to be negative 
            if (differences < 0)
                differences = -differences;
            
            Debug.Log($"The differences is {differences} {differences <= _trackResyncDistance}");
            if (differences <= _trackResyncDistance)
                return;
            
            _posOnTrack = distance;
            _angle = angle;
        }

        private IEnumerator ResetSpeed(float duration)
        {
            yield return new WaitForSeconds(duration);
            
            DOTween.To(
                () => _currentSpeed, 
                x => _currentSpeed = x, 
                _baseSpeed, 
                _resetTime);
        }

        [PunRPC]
        private void UpdateAngle(float newAngle)
        {
            if (_view.IsMine)
                return;

            _angle = newAngle;
        }

        private void FindOtherPlayers()
        {
            PlayerController[] all = FindObjectsOfType<PlayerController>();
            
            // The -1 is for removing this player controller
            _otherPlayers = new List<PlayerController>(all.Length - 1);

            // Removing this player controller from the array
            foreach (var player in all)
            {
                if (!player.View.IsMine)
                    continue;
                
                _otherPlayers.Add(player);
            }
        }

        public void ChangeGhostEffect(float newAmount)
        {
            foreach (MeshRenderer renderer in _renderers)
            {
                renderer.material.SetFloat("", newAmount);
            }
        }

        // This disables the control and invokes the listerens
        private void Finished()
        {
            IsActive = false;
            if (!_view.IsMine)
                return;
            OnFinished?.Invoke();
        }

    }
}