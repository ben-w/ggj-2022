using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class UpdateVolume : MonoBehaviour
    {
        [SerializeField]
        private AudioSource _source;
        
        private void OnEnable()
        {
            _source.volume = MusicManager.Volume;
            MusicManager.VolumeChanged += VolumeChanged;
        }

        private void VolumeChanged(float newVolume)
        {
            _source.volume = newVolume;
        }

        public void OnTriggerEnter(Collider other)
        {
            _source.Play();
        }

        private void OnDisable()
        {
            MusicManager.VolumeChanged -= VolumeChanged;
        }
    }

}