using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game
{
    public class TunnelGenerator : MonoBehaviour
    {
        
        [FormerlySerializedAs("_sections")]
        public List<GameObject> Sections = new List<GameObject>();
        
        [Header("Settings")]
        
        [SerializeField, Min(1)]
        private int _sectionsCount;

        [SerializeField, Min(0.1f)]
        private float _sectionWidth;

        [FormerlySerializedAs("_sectionLength"), Min(0.1f)]
        public float SectionLength = 100;

        [SerializeField, Min(0.1f)]
        private float _sectionHeight = 0.2f;

        [FormerlySerializedAs("_size"), Min(0.1f)]
        public float Size = 10;

        [SerializeField]
        private Material _sectionMaterial;

        [SerializeField]
        private Material _sectionMaterialAlt;

        [SerializeField]
        private Transform _transform;

#if UNITY_EDITOR
        private void OnValidate()
        {
            // There seems to be many issues with doing stuff on OnValidate,
            // so this adds a little delay to work around that
            UnityEditor.EditorApplication.delayCall += GenerateTunnel;
            
        }

        private void GenerateTunnel()
        {
            Queue<GameObject> _oldSections = new Queue<GameObject>();

            foreach (GameObject section in Sections)
            {
                _oldSections.Enqueue(section);
            }

            Sections = new List<GameObject>(_sectionsCount);

            float increment = 360f / _sectionsCount;
            

            for (int i = 0; i < _sectionsCount; i++)
            {
                GameObject currentObject = _oldSections.Count > 0
                    ? _oldSections.Dequeue()
                    : GameObject.CreatePrimitive(PrimitiveType.Cube);
                Transform transform = currentObject.transform;
                transform.SetParent(_transform);

                transform.localScale = new Vector3(_sectionHeight, _sectionWidth, SectionLength);
                
                float angle = i * increment;
                // Trigonometry
                var radians = angle * Mathf.Deg2Rad;
                var x = Mathf.Cos(radians);
                var y = Mathf.Sin(radians);
                transform.position = new Vector3(x * Size, y * Size, SectionLength / 2f);
                
                transform.rotation = Quaternion.Euler(0,0,angle);
                
                // Changing material
                MeshRenderer renderer = currentObject.GetComponent<MeshRenderer>();
                renderer.material = i % 2 == 1 ? _sectionMaterial : _sectionMaterialAlt;
                Sections.Add(currentObject);
            }

            // Removing any left over game objects
            while (_oldSections.Count > 0)
            {
                DestroyImmediate(_oldSections.Dequeue());
            }
            
        }
#endif
    }
}