using System;
using System.Collections;
using System.Collections.Generic;
using BansheeGz.BGSpline.Components;
using Cinemachine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Game
{
    public class LobbyManager : MonoBehaviourPunCallbacks
    {
        private const string STARTTIME = "StartTime";

        [SerializeField]
        private int _countDownTimer = 5;
        
        [SerializeField]
        private GameObject _lobbyCanvas;

        [SerializeField]
        private GameObject _gameCanvas;

        [SerializeField]
        private GameObject _finishedCanvas;

        [SerializeField]
        private string _playerPrefabPath;

        [SerializeField]
        private TextMeshProUGUI _countDownText;
        
        [SerializeField]
        private TextMeshProUGUI _finishedText;

        [SerializeField]
        private BGCcMath _math;

        [SerializeField]
        private PhotonView _view;

        [SerializeField]
        private CinemachineVirtualCamera _virtualCamera;
        
        private double _timerStart;
        private double _timerDifference;
        
        private void Start()
        {
            GameObject player = PhotonNetwork.Instantiate(_playerPrefabPath, new Vector3(0,0,11), Quaternion.identity);
            PlayerController controller = player.GetComponent<PlayerController>();
            PlayerColour colour = player.GetComponent<PlayerColour>();
            
            colour.SetMaterial(PhotonNetwork.LocalPlayer.IsMasterClient);
            controller.Curve = _math;
            _virtualCamera.Follow = player.transform;
            _virtualCamera.LookAt = player.transform;
            controller.OnFinished += LocalPlayerFinished;
        }

        public void EveryoneReady()
        {
            // Disabling all the stuff we don't use now
            _lobbyCanvas.SetActive(false);
            _gameCanvas.SetActive(true);

            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                var table = new Hashtable
                {
                    { STARTTIME, PhotonNetwork.Time }
                };
                PhotonNetwork.CurrentRoom.SetCustomProperties(table);
            }
        }

        // This will also get called on the master client, who changed it.
        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
        {
            Debug.Log($"Custom Properties Changed");
            
            if (!propertiesThatChanged.ContainsKey(STARTTIME))
                return;

            _timerStart = (double)propertiesThatChanged[STARTTIME];
            Debug.Log($"Start Time is {_timerStart} and Photon Time is {PhotonNetwork.Time}");
            StartCoroutine(CountDown());
        }

        private IEnumerator CountDown()
        {
            while (_timerDifference < _countDownTimer)
            {
                _timerDifference = PhotonNetwork.Time - _timerStart;
                double time = _countDownTimer - _timerDifference;
                _countDownText.text = $"{time:N1}";
                yield return new WaitForEndOfFrame();
            }
            Debug.Log("Timer Finished " + DateTime.Now.Millisecond);
            _countDownText.text = "Go!";
            ChangePlayersState(true);
            yield return new WaitForSeconds(0.5f);
            _countDownText.enabled = false;
        }

        private void ChangePlayersState(bool state)
        {
            var players = FindObjectsOfType<PlayerController>();

            foreach (PlayerController player in players)
            {
                player.IsActive = state;
            }
        }

        private void LocalPlayerFinished()
        {
            _finishedCanvas.SetActive(true);
            _finishedText.text = "You won!";
            _view.RPC(nameof(OtherPlayerFinished), RpcTarget.Others);
            
            ChangePlayersState(false);
        }

        [PunRPC]
        private void OtherPlayerFinished()
        {
            _finishedCanvas.SetActive(true);
            _finishedText.text = "You lost.";
            
            ChangePlayersState(false);
        }
    }
}