using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{
    private const string VOLUMEKEY = "Music Volume";
    public static Action<float> VolumeChanged;
    public static float Volume;

    [SerializeField]
    private Slider _slider;
    
    [SerializeField]
    private AudioSource _source;

    private void Awake()
    {
        float volume = PlayerPrefs.GetFloat(VOLUMEKEY, 0.5f);
        Volume = volume;
        _slider.value = volume;
        _source.volume = volume;
        _source.Play();
    }

    public void SliderChanged(float newValue)
    {
        Debug.Log($"Changing music volume from {_source.volume} to {newValue}");
        _source.volume = newValue;
        PlayerPrefs.SetFloat(VOLUMEKEY, newValue);
        PlayerPrefs.Save();
        Volume = newValue;
        VolumeChanged?.Invoke(newValue);
    }
}
