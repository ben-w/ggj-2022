using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CustomButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public UnityEvent OnClicked;
    
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            _isEnabled = value;
            
            if (_text == null)
                return;
            _text.color = value ? _normalColour : _disabledColour;
        }
    }
    

    [SerializeField]
    private Color _hoverColour;

    [SerializeField]
    private Color _normalColour;
    
    [SerializeField]
    private Color _disabledColour;

    [SerializeField]
    private TextMeshProUGUI _text;

    [SerializeField]
    private bool _isEnabled = true;
    
    private void Awake()
    {
        _text.color = IsEnabled ? _normalColour : _disabledColour;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _text.color = IsEnabled ? _hoverColour : _disabledColour;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _text.color = IsEnabled ? _normalColour : _disabledColour;
    }

    private void OnValidate()
    {
        if (_text == null)
            return;
        
        _text.color = IsEnabled ? _normalColour : _disabledColour;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!IsEnabled)
            return;
        
        Debug.Log($"{name} was clicked");
        OnClicked.Invoke();
    }
}
